#!/usr/bin/python3

from Ator import Ator


def main():

    print("Bem-vindo a franquia John Wick.")
    print("Cuidado para nao aborrecer John")

    opt = 0

    pontos = 0
    quizz = False

    while opt != -1:

        print("\nEscolha uma opcao:")
        print("1 - Informacoes sobre o filme")
        print("2 - Faca o quiz")
        print("Digite -1 pra sair\n")

        try:
            opt = int(input("Escolha sua opcao: "))
        except ValueError:
            pass

        if opt == 1:
            info()
        elif opt == 2 and not quizz:
            quizz = True
            pontos = quiz()
        elif opt == -1 and quizz:
            print("\nParabens. voce completou o quiz e fez {} ponto(s)".format(pontos))
            print("Obrigado. Ate a proxima!")
            print("Tchau! :)")
        elif opt == -1 and not quizz:
            print("Tchau! :)")
        else:
            print("\nERRO: Digite uma opcao valida\n")


def info():
    opt = 0

    while opt != -1:
        print("\nEscolha uma opcao:")
        print("1 - John Wick")
        print("2 - Ms. Perkins")
        print("3 - Winston")
        print("4 - Doguinho")
        print("Digite -1 para ir ao menu principal\n")

        try:
            opt = int(input("Escolha sua opcao: "))
        except ValueError:
            pass

        if opt == -1:
            pass
        elif opt == 1:
            wick = Ator("John Wick",
                         "E um lendario assassino de aluguel aposentado, "
                         "lidando com o luto apos perder o grande amor de sua vida.")
            print()
            print(wick.get_nome())
            print(wick.get_info())
            print()
        elif opt == 2:
            perk = Ator("Ms. Perkins",
                         "Uma assassina e antiga conhecida de John que tinha tomado o contrato para matá-lo")
            print()
            print(perk.get_nome())
            print(perk.get_info())
            print()
        elif opt == 3:
            wins = Ator("Winston",
                         "O dono do Hotel Continental em Nova Iorque, e proibido fazer negocios nesse lugar.")
            print()
            print(wins.get_nome())
            print(wins.get_info())
            print()
        elif opt == 4:
            dogg = Ator("Doguinho",
                         "Vitima do primeiro crime do filme, foi um presente dado a John pela sua falecida esposa")
            print()
            print(dogg.get_nome())
            print(dogg.get_info())
            print()
        else:
            print("\nERRO: Digite uma opcao valida\n")


def quiz():
    pontos = 0
    resposta = 0

    print("\nBem-vindo ao quiz sobre John Wick. Boa sorte!")

    # PERGUNTA 1
    print("\n1 - Qual o nome do ator principal do filme?")
    print("a) John Band")
    print("b) Cell Live")
    print("c) Keanu Reeves")
    print("d) Victor Lennon")
    resposta = input("\nResposta: ")

    if resposta.lower() == 'c':
        print("\nACERTOU!")
        pontos += 1
    else:
        print("ERRADO! Verifique sua resposta :(")

    # PERGUNTA 2
    print("\n2 - Qual o nome do dono do Contineltal em Nova Iorque?")
    print("a) Faber")
    print("b) Winston")
    print("c) Keanu")
    print("d) Victor")
    resposta = input("\nResposta: ")

    if resposta.lower() == 'b':
        print("\nACERTOU!")
        pontos += 1
    else:
        print("ERRADO! Verifique sua resposta :(")

    # PERGUNTA 3
    print("\n3 - Qual ator de Matrix aparece no filme John Wick, alem do Keanu")
    print("a) Laurence Fishburne")
    print("b) Trinity")
    print("c) Johnny Fish")
    print("d) Victor Lennon")
    resposta = input("\nResposta: ")

    if resposta.lower() == 'a':
        print("\nACERTOU!")
        pontos += 1
    else:
        print("ERRADO! Verifique sua resposta :(")

    # PERGUNTA 4
    print("\n4 - Quantos filmes foram lancados na franquia?")
    print("a) 2")
    print("b) 1")
    print("c) 4")
    print("d) 3")
    resposta = input("\nResposta: ")

    if resposta.lower() == 'd':
        print("\nACERTOU!")
        pontos += 1
    else:
        print("ERRADO! Verifique sua resposta :(")

    # PERGUNTA 5
    print("\n5 - Em que ano foi lancado o primeiro filme")
    print("a) 2015")
    print("b) 2014")
    print("c) 2013")
    print("d) 2012")
    resposta = input("\nResposta: ")

    if resposta.lower() == 'b':
        print("\nACERTOU!")
        pontos += 1
    else:
        print("ERRADO! Verifique sua resposta :(")

    return pontos


if __name__ == '__main__':
    main()